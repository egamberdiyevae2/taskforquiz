import React, { useEffect, useReducer, useState } from "react";
import Header from "./Header";
import Main from "./Main";
import Loader from "./Loader";
import Error from "./Error";
import StartScreen from "./StartScreen";
import Questions from "./Questions";
import NextButton from "./NextButton";
import Finished from "./Finished";
import Progress from "./Progress";
import Timer from "./Timer";
const SEC_FOR_QUESTIONS = 30;
const initialState = {
  questions: [],
  status: "loading",
  index: 0,
  point: 0,
  answer: null,
  secondRemaining: 10,
};
//*reducer funksiyasi 2 ta parametr qabul qiladi 1-si usereducer dagi state 2-si dispatchdan keladigan qiymatlar objecti
function reducer(state, action) {
  switch (action.type) {
    case "dataReceive":
      return {
        ...state,
        questions: action.payload,
        status: "ready",
      };
    case "error":
      return {
        ...state,
        questions: action.payload,
        status: "error",
      };
    case "start":
      return {
        ...state,
        status: "active",
        secondRemaining: state.questions.length * SEC_FOR_QUESTIONS,
      };
    case "newAnswer":
      const question = state.questions.at(state.index);
      console.log(question);
      console.log(action.payload);
      return {
        ...state,
        answer: action.payload,
        point:
          action.payload == question.correctOption
            ? state.point + question.points
            : state.point,
      };
    case "nextQuestions":
      return {
        ...state,
        index: state.index + 1,
        answer: null,
      };
    case "finished":
      return {
        ...state,
        status: "finish",
      };
    case "restart":
      return {
        ...initialState,
        questions: state.question,
        status: "ready",
      };
    case "tick":
      return {
        ...state,
        secondRemaining: state.secondRemaining - 1,
        status: state.secondRemaining == 0 ? "finish" : state.status,
      };
    default:
      throw new Error("This action is undefined");
  }
}
const App = () => {
  const [
    { questions, status, index, point, answer, secondRemaining },
    dispatch,
  ] = useReducer(reducer, initialState);
  const numberOfQuestions = questions.length;
  let maxPossiblePoints = questions.reduce((prev, cur) => prev + cur.points, 0);
  useEffect(() => {
    fetch("http://localhost:8080/questions")
      .then((res) => res.json())
      .then((data) => dispatch({ type: "dataReceive", payload: data }))
      .catch((err) => dispatch({ type: "error", payload: err.massage }));
  }, []);

  return (
    <div className="app">
      <Header />
      <Main>
        {status == "loading" && <Loader />}
        {status == "error" && <Error />}
        {status == "ready" && (
          <StartScreen questions={numberOfQuestions} dispatch={dispatch} />
        )}
        {status == "active" && (
          <>
            <Progress
              index={index}
              point={point}
              numberOfQuestions={numberOfQuestions}
              maxPossiblePoints={maxPossiblePoints}
            />
            <Questions
              questions={questions[index]}
              dispatch={dispatch}
              answer={answer}
            />
            <Timer secondRemaining={secondRemaining} dispatch={dispatch} />
            <NextButton
              dispatch={dispatch}
              index={index}
              numberOfQuestions={numberOfQuestions}
              answer={answer}
            />
          </>
        )}
        {status == "finish" && (
          <Finished
            point={point}
            maxPossiblePoints={maxPossiblePoints}
            dispatch={dispatch}
          />
        )}
      </Main>
    </div>
  );
};

export default App;
