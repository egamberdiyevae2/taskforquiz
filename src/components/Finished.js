import React from "react";

const Finished = ({ point, maxPossiblePoints, dispatch }) => {
  return (
    <>
      <h1>
        Finished with {point} out of {maxPossiblePoints}
      </h1>
      <button
        className="btn btn-ui"
        onClick={() => dispatch({ type: "restart" })}
      >
        Restart
      </button>
    </>
  );
};

export default Finished;
