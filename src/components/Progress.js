import React from "react";

const Progress = ({ index, numberOfQuestions, point, maxPossiblePoints }) => {
  return (
    <header className="progress">
      <progress max={numberOfQuestions} value={index} />
      <p>
        Questions <strong>{index + 1}</strong> / {numberOfQuestions}
      </p>
      <p>
        <strong>
          {point} / {maxPossiblePoints}
        </strong>
      </p>
    </header>
  );
};

export default Progress;
