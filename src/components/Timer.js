import React, { useEffect } from "react";

const Timer = ({ secondRemaining, dispatch }) => {
  let mins = Math.floor(secondRemaining / 60);
  let second = Math.floor(secondRemaining % 60);
  useEffect(() => {
    const id = setInterval(() => {
      //   console.log("tick");
      dispatch({ type: "tick" });
    }, 1000);
    return () => {
      clearInterval(id);
    };
  }, [dispatch]);
  return (
    <div className="timer">
      {mins} : {second}
    </div>
  );
};

export default Timer;
